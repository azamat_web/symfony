<?php

namespace App\Controller;

use App\Entity\Calendar;
use App\Entity\Status;
use App\Entity\User;
use App\Form\CalendarType;
use App\Repository\CalendarRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
/**
 * @Route("api/calendars")
 */
class CalendarController extends Controller
{
    /**
     * @Route("/", name="calendar_index", methods="GET")
     */
    public function index(CalendarRepository $calendarRepository): Response
    {
        $data = null;
        foreach ($calendarRepository->findAll() as $key => $item){

            $data[$key]['id'] = $item->getId();
            $data[$key]['status'] = $item->getStatus()->getTitle();
            $data[$key]['user'] = $item->getUser()->getFirstname() .' '. $item->getUser()->getLastname();
            $data[$key]['startDate'] = $item->getStartDate()->format('Y-m-d H:i');
            $data[$key]['endDate'] = $item->getEndDate()->format('Y-m-d H:i');

        }
        return $this->json($data);
    }

    /**
     * @Route("/new", name="calendar_new", methods="GET|POST")
     */
    public function new(Request $request): Response
    {
        $calendar = new Calendar();
        if ($request->get('startDate') == null || $request->get('endDate') == null){
            return $this->json([
                'status' => Response::HTTP_BAD_REQUEST,
                'message' => 'empty value! startDate or endDate'
            ]);
        }
        $form = $this->createForm(CalendarType::class, $calendar);
        $form->handleRequest($request);
        $form->submit($request->request->all());
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($calendar);
            $em->flush();
            return $this->json([
                'status' => Response::HTTP_CREATED,
                'message' => 'success'
            ]);
        }
        return $this->json([
            'status' => Response::HTTP_BAD_REQUEST,
            'message' => 'error'
        ]);
    }

    /**
     * @Route("/{id}", name="calendar_show", requirements={"id" = "\d+"}, methods="GET")
     */
    public function show(CalendarRepository $calendarRepository, $id): Response
    {
        $data = null;
        $calendar = $calendarRepository->find($id);
        if ($calendar){
            $data['status'] = $calendar->getStatus()->getTitle();
            $data['user'] = $calendar->getUser()->getFirstname() .' '. $calendar->getUser()->getLastname();
            $data['startDate'] = $calendar->getStartDate()->format('Y-m-d H:i');
            $data['endDate'] = $calendar->getEndDate()->format('Y-m-d H:i');

            return $this->json($data);
        }
        return $this->json([
            'status' => Response::HTTP_NOT_FOUND,
            'message' => 'not found'
        ]);
    }

    /**
     * @Route("/{id}/edit", name="calendar_edit", methods="GET|POST")
     */
    public function edit(Request $request, Calendar $calendar): Response
    {
        $form = $this->createForm(CalendarType::class, $calendar);
        $form->handleRequest($request);
        $form->submit($request->request->all());

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->json([
                'status' => Response::HTTP_OK,
                'message' => 'updated'
            ]);
        }

        return $this->json([
            'status' => Response::HTTP_BAD_REQUEST,
            'message' => 'error'
        ]);
    }

    /**
     * @Route("/{id}", name="calendar_delete", methods="DELETE")
     */
    public function delete(Request $request): Response
    {
        $status = $this->getDoctrine()->getRepository(Calendar::class) ->find($request->get('id'));
        if ($status){
            $em = $this->getDoctrine()->getManager();
            $em->remove($status);
            $em->flush();

            return $this->json([
                'status' => Response::HTTP_OK,
                'message' => 'delete'
            ]);
        }
        return $this->json([
            'status' => Response::HTTP_BAD_REQUEST,
            'message' => 'error'
        ]);
    }

    /**
     * @Route("/filterBy/{field}", name="calendar_filter_user", requirements={"field": "user|date"}, methods="GET")
     */
    public function filterByUser(CalendarRepository $calendarRepository, $field): Response
    {
        $calendars = null;
        $data = null;
        if ($field == 'user'){
            $calendars = $calendarRepository->findBy(array(), array('user' => 'ASC'));
        }
        elseif ($field == 'date'){
            $calendars = $calendarRepository->findBy(array(), array('startDate' => 'ASC', 'endDate' => 'ASC'));
        }
        if ($calendars){
            foreach ($calendars as $key => $item){
                $data[$key]['id'] = $item->getId();
                $data[$key]['status'] = $item->getStatus()->getTitle();
                $data[$key]['user'] = $item->getUser()->getFirstname() .' '. $item->getUser()->getLastname();
                $data[$key]['startDate'] = $item->getStartDate()->format('Y-m-d H:i');
                $data[$key]['endDate'] = $item->getEndDate()->format('Y-m-d H:i');
            }
            return $this->json($data);
        }
        return $this->json([
            'status' => Response::HTTP_NOT_FOUND,
            'message' => 'not found'
        ]);
    }
}
