<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/users")
 */
class UserController extends Controller
{
    /**
     * @Route("/", name="user_index", methods="GET")
     */
    public function index(UserRepository $userRepository): Response
    {
        return $this->json($userRepository->findAll());
    }

    /**
     * @Route("/new", name="user_new", methods="GET|POST")
     */
    public function new(Request $request): Response
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);
        $form->submit($request->request->all());

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            return $this->json([
                'status' => Response::HTTP_CREATED,
                'message' => 'success'
            ]);
        }
        return $this->json([
            'status' => Response::HTTP_BAD_REQUEST,
            'message' => 'error'
        ]);
    }

    /**
     * @Route("/{id}", name="user_show", requirements={"id" = "\d+"}, methods="GET")
     */
    public function show(User $user): Response
    {
        return $this->json($user);
    }

    /**
     * @Route("/{id}/edit", name="user_edit", methods="GET|POST")
     */
    public function edit(Request $request, User $user): Response
    {
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);
        $form->submit($request->request->all());
        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->json([
                'status' => Response::HTTP_OK,
                'message' => 'updated'
            ]);
        }

        return $this->json([
            'status' => Response::HTTP_BAD_REQUEST,
            'message' => 'error'
        ]);
    }

    /**
     * @Route("/{id}", name="user_delete", methods="DELETE")
     */
    public function delete(Request $request): Response
    {

        $user = $this->getDoctrine()->getRepository(User::class) ->find($request->get('id'));
        if ($user){
            $em = $this->getDoctrine()->getManager();
            $em->remove($user);
            $em->flush();

            return $this->json([
                'status' => Response::HTTP_OK,
                'message' => 'delete'
            ]);
        }
        return $this->json([
            'status' => Response::HTTP_BAD_REQUEST,
            'message' => 'error'
        ]);

    }

    /**
     * @Route("/{firstName}/{lastName}", name="user_show_by_user_name", methods="GET")
     */
    public function showByFullName(UserRepository $userRepository, $firstName, $lastName): Response
    {
        $user = $userRepository->findOneBy([
            'firstname' => $firstName,
            'lastname' => $lastName,
        ]);

        if ($user){
            return $this->json($user);
        }
        return $this->json([
            'status' => Response::HTTP_NOT_FOUND,
            'message' => 'not found'
        ]);
    }
}
