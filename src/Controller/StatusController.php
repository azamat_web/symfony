<?php

namespace App\Controller;

use App\Entity\Status;
use App\Form\StatusType;
use App\Repository\StatusRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/statuses")
 */
class StatusController extends Controller
{
    /**
     * @Route("/", name="status_index", methods="GET")
     */
    public function index(StatusRepository $statusRepository): Response
    {
        return $this->json($statusRepository->findAll());
    }

    /**
     * @Route("/new", name="status_new", methods="GET|POST")
     */
    public function new(Request $request): Response
    {
        $status = new Status();
        $form = $this->createForm(StatusType::class, $status);
        $form->handleRequest($request);
        $form->submit($request->request->all());

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($status);
            $em->flush();

            return $this->json([
                'status' => Response::HTTP_CREATED,
                'message' => 'success'
            ]);
        }

        return $this->json([
            'status' => Response::HTTP_BAD_REQUEST,
            'message' => 'error'
        ]);
    }

    /**
     * @Route("/{id}", name="status_show", methods="GET")
     */
    public function show(Status $status): Response
    {
        return $this->json($status);
    }

    /**
     * @Route("/{id}/edit", name="status_edit", methods="GET|POST")
     */
    public function edit(Request $request, Status $status): Response
    {
        $form = $this->createForm(StatusType::class, $status);
        $form->handleRequest($request);
        $form->submit($request->request->all());

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->json([
                'status' => Response::HTTP_OK,
                'message' => 'updated'
            ]);
        }

        return $this->json([
            'status' => Response::HTTP_BAD_REQUEST,
            'message' => 'error'
        ]);
    }

    /**
     * @Route("/{id}", name="status_delete", methods="DELETE")
     */
    public function delete(Request $request): Response
    {
        $status = $this->getDoctrine()->getRepository(Status::class) ->find($request->get('id'));
        if ($status){
            $em = $this->getDoctrine()->getManager();
            $em->remove($status);
            $em->flush();

            return $this->json([
                'status' => Response::HTTP_OK,
                'message' => 'delete'
            ]);
        }
        return $this->json([
            'status' => Response::HTTP_BAD_REQUEST,
            'message' => 'error'
        ]);
    }
}
