<?php

namespace App\Form;

use App\Entity\Calendar;
use App\Entity\Status;
use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CalendarType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('startDate', DateTimeType::class, [
                'format'=>'yyyy-MM-dd HH:mm',
                'widget'=>'single_text'
            ])
            ->add('endDate', DateTimeType::class, [
                'format'=>'yyyy-MM-dd HH:mm',
                'widget'=>'single_text'
            ])
            ->add('user', EntityType::class, [
                'class' => User::class,
                'choice_label' => function ($user) {
                    return $user->getFirstname() .' '. $user->getLastname();
                }
            ])
            ->add('status', EntityType::class, [
                'class' => Status::class,
                'choice_label' => 'title',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Calendar::class,
            'csrf_protection' => false,
        ]);
    }
}
